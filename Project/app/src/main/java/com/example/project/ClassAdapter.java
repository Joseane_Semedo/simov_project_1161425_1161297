package com.example.project;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.ClassVH> {
    List<Lesson> LessonList;

    public ClassAdapter(List<Lesson> LessonList) {
        this.LessonList = LessonList;
    }

    @NonNull
    @Override
    public ClassAdapter.ClassVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_home, parent, false);
        return new ClassAdapter.ClassVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClassAdapter.ClassVH holder, int position) {

        Lesson aLesson = LessonList.get(position);
        holder.txtNameH.setText(aLesson.getSubject());
        holder.txtDecriptionH.setText(aLesson.getDescription());
        holder.txtTutorH.setText(aLesson.getTutor().getEmail());
    }


    @Override
    public int getItemCount() {
        return LessonList.size();
    }

    class ClassVH extends RecyclerView.ViewHolder {

        private static final String TAG = "LessonVH";

        ConstraintLayout expandableLayout;
        TextView txtNameH, txtDecriptionH, txtTutorH;

        public ClassVH(@NonNull final View itemView) {
            super(itemView);

            txtNameH = itemView.findViewById(R.id.txtNameH);
            txtDecriptionH = itemView.findViewById(R.id.txtDecriptionH);
            txtTutorH = itemView.findViewById(R.id.txtTutorH);
        }
    }
}

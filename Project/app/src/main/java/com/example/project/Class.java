package com.example.project;

public class Class {

    private String name;
    private String description;
    private boolean expanded;

    public Class(String name, String description) {
        this.name = name;
        this.description = description;
        this.expanded = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    @Override
    public String toString() {
        return "Class{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", expanded=" + expanded +
                '}';
    }
}

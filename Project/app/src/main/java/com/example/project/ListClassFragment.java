package com.example.project;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListClassFragment extends Fragment {
    private Context context = getContext();
    private RecyclerView recyclerView;
    private List<Lesson> classList;
    private DatabaseReference myRef;
    private FirebaseDatabase myDatabase;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_class, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = getView().findViewById(R.id.recyclerView);
        initData();
    }

    private void initRecyclerView(List<Lesson> allData) {
        LessonAdapter lessonAdapter = new LessonAdapter(allData);
        lessonAdapter.notifyDataSetChanged();
        recyclerView.setLayoutManager(new LinearLayoutManager(this.context));
        recyclerView.setAdapter(lessonAdapter);
    }

    private void initData() {
        final User tutor = getTutor();
        classList = new ArrayList<>();
        DatabaseReference database = myDatabase.getInstance().getReference();
        myRef = database.child("lessons");


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot data : snapshot.getChildren()) {
                     if(data.child("tutor").child("id").getValue().toString().equals(tutor.getID())) {
                         Lesson lesson = data.getValue(Lesson.class);
                         Log.e("ListClassFragment", "Lesson:" + lesson);
                         classList.add(lesson);
                     }
                }

                initRecyclerView(classList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private User getTutor() {
        user = mAuth.getCurrentUser();
        String name = user.getDisplayName();
        String email = user.getEmail();
        String id = user.getUid();

        User tutor = new User();
        tutor.setName(name);
        tutor.setEmail(email);
        tutor.setID(id);

        return tutor;
    }
}

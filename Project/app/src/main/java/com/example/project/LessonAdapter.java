package com.example.project;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.LessonVH> {

    private static final String TAG = "LessonAdapter";
    List<Lesson> LessonList;

    public LessonAdapter(List<Lesson> LessonList) {
        this.LessonList = LessonList;
    }

    @NonNull
    @Override
    public LessonVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_class_row, parent, false);
        return new LessonVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LessonVH holder, int position) {

        Lesson aLesson = LessonList.get(position);
        holder.titleTextView.setText(aLesson.getSubject());
        holder.descriptionTextView.setText(aLesson.getDescription());

        boolean isExpanded = LessonList.get(position).isExpanded();
        holder.expandableLayout.setVisibility(isExpanded ? View.VISIBLE : View.GONE);

    }

    @Override
    public int getItemCount() {
        return LessonList.size();
    }

    class LessonVH extends RecyclerView.ViewHolder {

        private static final String TAG = "LessonVH";

        ConstraintLayout expandableLayout;
        TextView titleTextView, descriptionTextView;

        public LessonVH(@NonNull final View itemView) {
            super(itemView);

            titleTextView = itemView.findViewById(R.id.titleTextView);
            descriptionTextView = itemView.findViewById(R.id.descriptionTextView);
            expandableLayout = itemView.findViewById(R.id.expandableLayout);


            titleTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Lesson aLesson = LessonList.get(getAdapterPosition());
                    aLesson.setExpanded(!aLesson.isExpanded());
                    notifyItemChanged(getAdapterPosition());

                }
            });
        }
    }
}

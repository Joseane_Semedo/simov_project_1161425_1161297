package com.example.project;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

public class LoginActivity extends AppCompatActivity {
    String password;
    String email;

    private EditText textview_email;
    private EditText textview_password;

    private FirebaseAuth mAuth;
    private static final String TAG = "LoginActivity:";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();

        // User is logged in
        if (mAuth.getCurrentUser() != null) {
            //startActivity(new Intent(LoginActivity.this, HomeTeacherActivity.class));
            //startActivity(new Intent(LoginActivity.this, MainActivity.class));

            //startActivity(new Intent(LoginActivity.this, UserProfile.class));
            FirebaseUser user = mAuth.getCurrentUser();
            updateUI(user);
            finish();
        }

        textview_email = (EditText) findViewById(R.id.et_email);
        textview_password = (EditText) findViewById(R.id.et_password);

        Button signup_btn = findViewById(R.id.btn_signup);
        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(i);
            }
        });

        Button login_btn = findViewById(R.id.btn_login);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = textview_email.getText().toString();
                password = textview_password.getText().toString();

                if (validateForm()) {
                    signIn(email, password);
                }
            }
        });
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(getApplicationContext(),
                                    "Incorrect email address and / or password", Toast.LENGTH_LONG).show();
                            //updateUI(null);
                            //checkForMultiFactorFailure(task.getException());
                        }
                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference myRef = database.child("users").child(user.getUid());


        if (user != null) {

            myRef.addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User u = snapshot.getValue(User.class);
                    startActivity(u.getType());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                }

            });
        }

        /*if (user != null) {
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            //Intent i = new Intent(LoginActivity.this, UserProfile.class);
            startActivity(i);
        }*/
    }

    private boolean validateForm() {
        boolean valid = true;

        if (email.isEmpty()) {
            textview_email.setError("Please enter a valid email");
            valid = false;
        }
        if (password.isEmpty()) {
            textview_password.setError("Please enter a valid password");
            valid = false;
        }
        return valid;
    }

    private void startActivity(String type){
        Intent i = type.equals("Teach") ?
                new Intent(LoginActivity.this, HomeTeacherActivity.class) :
                new Intent(LoginActivity.this, MainActivity.class);

        startActivity(i);
    }

    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
    }
}
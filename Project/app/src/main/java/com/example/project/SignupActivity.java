package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;

public class SignupActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    private StorageReference mStorageRef;
    private FirebaseDatabase mDatabase;
    private DatabaseReference myRef;
    private static final String TAG = "SignupActivity:";

    String name;
    String email;
    String password;
    String repassword;
    String type = "";

    private EditText textview_name;
    private EditText textview_email;
    private EditText textview_password;
    private EditText textview_repassword;

    private CheckBox checkBoxTeach;
    private CheckBox checkBoxStudent;

    private Uri pic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mAuth = FirebaseAuth.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();


        textview_name = (EditText) findViewById(R.id.et_name);
        textview_email = (EditText) findViewById(R.id.et_email);
        textview_password = (EditText) findViewById(R.id.et_password);
        textview_repassword = (EditText) findViewById(R.id.et_repassword);


        checkBoxStudent = (CheckBox) findViewById(R.id.checkBoxStudent);
        checkBoxTeach = (CheckBox) findViewById(R.id.checkBoxTeach);

        checkBoxStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBoxStudent.isChecked()) {
                    type = "Student";
                } else {
                    type = "";
                }
            }
        });


        checkBoxTeach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBoxTeach.isChecked()) {
                    type = "Teach";
                } else {
                    type = "";
                }
            }
        });


        Button btn_register = findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = textview_name.getText().toString();
                email = textview_email.getText().toString();
                password = textview_password.getText().toString();
                repassword = textview_repassword.getText().toString();

                if (validateForm()) {
/*
                    createAccount(name, email, password, type);


                    Intent i = new Intent(SignupActivity.this, MainActivity.class);
                    startActivity(i);
*/
                    createAccount(name,email, password, type);
                    startActivity(type);
                }
            }
        });
    }

    private void createAccount(final String name, final String email, final String password, final String type) {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference myRef = database.child("users");


        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            updateProfile(name);

                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            User u1 = new User();
                            u1.setEmail(email);
                            u1.setID(user.getUid());
                            u1.setName(name);
                            u1.setType(type);

                            Log.d(TAG, "User:" + u1);
                            myRef.child(user.getUid()).setValue(u1);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignupActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                    }
                });
    }

    public void updateProfile(String name) {
        //mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .build();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User profile updated.");
                        }
                    }
                });

        //Toast.makeText(getApplicationContext(), "namess updated", Toast.LENGTH_LONG).show();
    }

    private boolean validateForm() {
        boolean valid = true;

        if (email.isEmpty()) {
            textview_email.setError("Please enter a valid email");
            valid = false;
        }
        if (name.isEmpty()) {
            textview_name.setError("Please enter a valid name");
            valid = false;
        }
        if (repassword.isEmpty()) {
            textview_password.setError("Please enter a valid password");
            valid = false;
        }
        if (password.isEmpty()) {
            textview_password.setError("Please enter a valid password");
            valid = false;
        }
        if (password.length() < 6) {
            textview_password.setError("Password must be at least six characters long");
            valid = false;
        }
        if (!password.equals(repassword)) {
            textview_repassword.setError("Password doesn't match");
            valid = false;
        }

        if (type.isEmpty()) {
            checkBoxStudent.setError("Please select checkbox");
            checkBoxTeach.setError("Please select checkbox");
            valid = false;
        }

        if (checkBoxStudent.isChecked() == checkBoxTeach.isChecked()) {
            checkBoxStudent.setError("Please select only one");
            checkBoxTeach.setError("Please select only one");
            valid = false;
        }

        return valid;
    }


    //public void updateProfile(String name) throws IOException {
    //Uri defaultPic = Uri.fromFile(new File("Project/app/src/main/res/drawable/image.jpg"));
        /*StorageReference picRef = mStorageRef.child("images/test.jpg");

        File localFile = File.createTempFile("images", "jpg");
        picRef.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        // Successfully downloaded data to local file
                        // ...
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle failed download
                // ...
            }
        });*/

        /*mStorageRef.child("images/test.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                pic = uri;
                // Got the download URL for 'users/me/profile.png'
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .setPhotoUri(Uri.parse(pic.toString()))
                .build();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }*/

    private void startActivity(String type){
        Intent i = type.equals("Teach") ?
                new Intent(SignupActivity.this, HomeTeacherActivity.class) :
                new Intent(SignupActivity.this, MainActivity.class);

        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
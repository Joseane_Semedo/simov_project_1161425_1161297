package com.example.project;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@IgnoreExtraProperties
public class Lesson {
    private User tutor;
    private List<User> students;
    private List<String> links;
    private String subject;
    private String description;
    private int price;
    private boolean expanded;

    public Lesson(){}

    public Lesson(User tutor, List<User> students, String subject, String description, int price) {
        this.tutor = tutor;
        this.students = students;
        this.subject = subject;
        this.description = description;
        this.price = price;
        this.expanded = false;
    }

    public Lesson(User tutor, String subject, String description) {
        this.tutor = tutor;
        this.subject = subject;
        this.description = description;
    }


    public User getTutor() {
        return tutor;
    }

    public List<User> getStudents() {
        return students;
    }

    public String getSubject() {
        return subject;
    }

    public String getDescription() {
        return description;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public int getPrice() {
        return price;
    }

    public boolean addStudent(User student) {
        return students.add(student);
    }

    public boolean removeStudent(User student) {
        return students.remove(student);
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("tutor", tutor);
        result.put("students", students);
        result.put("subject", subject);
        result.put("description", description);
        result.put("price", price);
        result.put("expanded", expanded);

        return result;
    }
}

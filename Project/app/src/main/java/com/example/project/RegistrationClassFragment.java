package com.example.project;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.concurrent.Executor;

import javax.xml.transform.Result;

public class RegistrationClassFragment extends Fragment implements Executor{
    private Context context = getContext();
    private FirebaseDatabase mDatabase;
    private DatabaseReference myRef;

    private TextView txtNameClass;
    private TextView txtNameSubject;
    private TextView txtDescription;
    private Button btnRegister;

    private String name;
    private String subject;
    private String description;

    private FirebaseAuth mAuth;
    private FirebaseUser user;

    public RegistrationClassFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_registration_class, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        txtNameClass = (TextView) getView().findViewById(R.id.txtClass);
        txtNameSubject = (TextView) getView().findViewById(R.id.txtSubject);
        txtDescription = (TextView) getView().findViewById(R.id.txtDescription);
        btnRegister = (Button) getView().findViewById(R.id.btn_register_class);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = txtNameClass.getText().toString();
                subject = txtNameSubject.getText().toString();
                description = txtDescription.getText().toString();

                if (validateForm()) {
                    //registerClass(name, description);
                    registerLesson(name, subject, description);
                }
            }

        });

    }

    private boolean validateForm() {
        boolean valid = true;

        if (name.isEmpty()) {
            txtNameClass.setError("Please enter the name of class");
            valid = false;
        }
        if (subject.isEmpty()) {
            txtNameSubject.setError("Please enter the subject");
            valid = false;
        }
        if (description.isEmpty()) {
            txtDescription.setError("Please enter the description of class");
            valid = false;
        }

        return valid;
    }

    private void registerLesson(String name, String subject, String description) {
        DatabaseReference database = mDatabase.getInstance().getReference();
        myRef = database.child("lessons");

        User tutor = getTutor();

        Lesson l = new Lesson(tutor, name, description);

        //Push generate a new key UID.
        myRef.push().setValue(l).addOnSuccessListener(new OnSuccessListener<Void>(){
            @Override
            public void onSuccess(Void aVoid) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new RegistrationClassFragment()).commit();
            }
        });

        Toast.makeText(getActivity(), "Class registered ", Toast.LENGTH_SHORT).show();

    }

    private User getTutor() {
        user = mAuth.getCurrentUser();
        String name = user.getDisplayName();
        String email = user.getEmail();
        String id = user.getUid();

        User tutor = new User();
        tutor.setName(name);
        tutor.setEmail(email);
        tutor.setID(id);

        return tutor;
    }

    @Override
    public void execute(Runnable command) {

    }
}

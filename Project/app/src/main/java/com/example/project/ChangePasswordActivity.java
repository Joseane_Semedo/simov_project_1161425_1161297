package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangePasswordActivity extends AppCompatActivity {

    //private EditText textview_current_pwd;
    private EditText textview_new_pwd;
    private EditText textview_renew_pwd;
    private FirebaseAuth mAuth;
    private FirebaseUser user;

    private static final String TAG = "ChangePasswordActivity:";

    //String current_pwd;
    String new_pwd;
    String renew_pwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        //textview_current_pwd = (EditText) findViewById(R.id.et_currentpwd);
        textview_new_pwd = (EditText) findViewById(R.id.et_newpwd);
        textview_renew_pwd = (EditText) findViewById(R.id.et_renewpwd);

        Button btn_changepwd = findViewById(R.id.btn_changepassword);
        btn_changepwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new_pwd = textview_new_pwd.toString();
                renew_pwd = textview_renew_pwd.toString();

                Toast.makeText(getApplicationContext(),
                        "name " + user.getDisplayName(), Toast.LENGTH_LONG).show();
                //var newPassword = getASecureRandomPassword();

                if (validateForm()) {
                    Toast.makeText(getApplicationContext(),
                            "valid", Toast.LENGTH_LONG).show();

                    user.updatePassword(new_pwd).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "Password updated");
                                Toast.makeText(getApplicationContext(),
                                        "Password updated", Toast.LENGTH_LONG).show();
                                finish();
                            } else {
                                Log.d(TAG, "Error password not updated");
                            }
                        }
                    });
                }
            }
        });
    }

    private boolean validateForm() {
        boolean valid = true;

        if (new_pwd.length() == 0) {
            textview_new_pwd.setError("Please enter a valid password");
            valid = false;
        }
        if (renew_pwd.isEmpty()) {
            textview_renew_pwd.setError("Please enter a valid password");
            valid = false;
        }
        if (!new_pwd.equals(renew_pwd)) {
            textview_renew_pwd.setError("Passwords don't match");
            //Toast.makeText(getApplicationContext(), "renew" + new_pwd.toString(), Toast.LENGTH_LONG).show();
            valid = false;
        }
        if (new_pwd.length() < 6) {
            textview_new_pwd.setError("Password must be at least six characters long");
            valid = false;
        }
        return valid;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}